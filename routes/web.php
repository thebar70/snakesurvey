<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
})->name('index');

//Rutas para el usuario
Route::group(['prefix'=>'/users'],function(){
    Route::prefix('/persons')->group(function(){
        Route::get('/register','PersonController@create')->name('register_person');
        Route::post('/store','PersonController@store')->name('store_person');
        Route::get('/show','PersonController@show')->name('show_person');
        Route::get('/index','PersonController@index_user')->name('index_user');


        //Evaluations
        Route::post('/evaluation/store','EvaluationController@store')->name('store_evaluation');
        Route::get('/evaluations/{token}', function (Request $request) {
            return redirect()->route('resolve_evaluation',$request->token);
        })->name('create_evaluation')->middleware('signed');
        
      });
});

//Ruta de redireccion para url
Route::get('/evaluations/resolve/{token}','EvaluationController@create')->name('resolve_evaluation');
//Rutas para el dashboard
Route::group(['prefix'=>'/dashboard','middleware'=>'auth'],function(){
    Route::prefix('/test')->group(function(){
        Route::get('/create','TestController@create')->name('create_form');
        Route::post('/store','TestController@store')->name('store_test');
        Route::get('/list','TestController@index')->name('list_tests');
        Route::get('/show/{id}','TestController@show')->name('show_test');
        Route::get('/show_tests_by_name','TestController@show_by_name')->name('show_tests_by_name');
        Route::get('/index','PersonController@index_evaluator')->name('index_evaluator');
    });

    Route::prefix('/list')->group(function(){
        Route::get('/create','ListController@create')->name('create_list');
        Route::post('/store','ListController@store')->name('store_list');
        Route::get('/show/{id}','ListController@show')->name('show_list');
        Route::post('/delete/email','ListController@delete_email')->name('delete_email_list');
        Route::put('/update/email/{id}','ListController@update_email')->name('update_email_list');
        //json
        Route::get('/obtener_lista','ListController@obtenerLista')->name('obtener_lista');
    });

    Route::prefix('/statistics')->group(function(){
        Route::get('/show','StatisticsController@show')->name('show_statistics');
        Route::post('/delete/email','ListController@delete_email')->name('delete_email_list');
        Route::put('/update/email/{id}','ListController@update_email')->name('update_email_list');
        //json
        Route::get('/obtener_lista','ListController@obtenerLista')->name('obtener_lista');
    });
    
});

/* //Rutas de acceso por token
Route::get('/users', function (Request $request) {
    if (! $request->hasValidSignature()) {
        abort(401);
    }

    Route::get('/evaluations','EvaluationController@resolve');
})->name('resolve_evaluation'); */

Auth::routes();
Route::get('auth/google', 'Auth\Socialite\LoginController@redirectToGoogle')->name('login_google');
Route::get('auth/google/callback', 'Auth\Socialite\LoginController@handleGoogleCallback');
Route::get('auth/facebook', 'Auth\Socialite\LoginController@redirectToFacebook')->name('login_facebook');
Route::get('auth/facebook/callback', 'Auth\Socialite\LoginController@handleFacebookCallback');

Route::get('/home', function () {
    return redirect()->route('index_evaluator');
})->name('home');




