<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable=['description'];
    protected $table='options';

    public function question(){
        return $this->belongsTo('App\Models\Question');
    }
    public function answers(){
        return $this->hasMany('App\Models\Answer');
    }
}
