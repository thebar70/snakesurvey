<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListEmail extends Model
{
    protected $fillable=['name'];
    protected $table='list_emails';

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function emails_user(){
        return $this->hasMany('App\Models\EmailUser');
    }
    public function tests(){
        return $this->hasMany('App\Models\Test');
    }
    


}
