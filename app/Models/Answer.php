<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable=['option_id','evaluation_id','value'];
    protected $table='answers';

    public function option(){
        return $this->belongsTo('App\Models\Option');
    }
    public function evaluation(){
        return $this->belongsTo('App\Models\Evaluation');
    }

}
