<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable=['form_id','user_id','list_email_id','site','date','name'];
    protected $table='tests';

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function form(){
        return $this->belongsTo('App\Models\Form');
    }
    public function evaluations(){
        return $this->hasMany('App\Models\Evaluation');
    }

    public function list_email(){
        return $this->belongsTo('App\Models\ListEmail');
    }
}
