<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable=['name'];
    protected $table='forms';

    public function tests(){
        return $this->hasMany('App\Models\Test');
    }
    public function questions(){
        return $this->hasMany('App\Models\Question');
    }
    public function site(){
        return $this->hasOne('App\Models\Site');
    }


}
