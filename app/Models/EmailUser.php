<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailUser extends Model
{
    protected $fillable=['email_user'];
    protected $table='email_users';

    public function list_email(){
        return $this->belongsTo('App\Models\ListEmail');
    }  

}
