<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable=['type','description'];
    protected $table='questions';

    public function forms(){
        return $this->belongsToMany('App\Models\Form','questions_forms');
    }
    public function options(){
        return $this->hasMany('App\Models\Option');
    }
}
