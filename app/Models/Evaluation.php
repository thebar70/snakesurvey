<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable=['token','birth_date','gender','user_email','test_id'];
    protected $table='evaluations';

    
    public function answers(){
        return $this->hasMany('App\Models\Answer');
    }
    public function test(){
        return $this->belongsTo('App\Models\Test');
    }


}
