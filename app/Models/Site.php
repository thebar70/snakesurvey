<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable=['name','url','description'];
    protected $table='sites';

    public function form(){
        return $this->belongsTo('App\Models\Form');
    }
}
