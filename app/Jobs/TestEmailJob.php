<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ActionClass\Dashboard\Mails\SendMailTest;

class TestEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $test;
    private $lista;
    public function __construct($test,$lista)
    {
        $this->test=$test;
        $this->lista=$lista;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SendMailTest::execute($this->test,$this->lista);
    }
}
