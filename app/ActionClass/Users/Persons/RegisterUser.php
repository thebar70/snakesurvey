<?php

namespace App\ActionClass\Users\Persons;

use Illuminate\Http\Request;
use App\Validators\PersonStoreValidator;
use App\Models\User;
use App\Models\Role;
use Auth;

class RegisterUser
{
    public static function execute(Request $data) 
    {
        $user = User::create([
            'names' => $data['names'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $user->roles()->attach(Role::where('name', 'evaluator')->first());
        Auth::login($user);
        return $user;

    }
}