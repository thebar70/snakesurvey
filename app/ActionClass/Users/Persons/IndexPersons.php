<?php

namespace App\ActionClass\Users\Persons;

use App\Result\Result;
use App\Models\Person;

class IndexPersons
{
    public static function execute() : Result
    {
        $persons=Person::paginate(20);
        if(!$persons->isEmpty()){
            $result = new Result(200, $person);
        }else{
            $result=new Result(202,'fails');
        }
        return $result;
    }
}