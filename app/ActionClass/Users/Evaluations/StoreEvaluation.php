<?php

namespace App\ActionClass\Users\Evaluations;

use App\Models\Answer;
use App\Models\Evaluation;
use App\Models\Question;
use App\Models\Option;
use App\Models\Test;


class StoreEvaluation
{
    public static function execute($request)
    {
        $answers = explode(',', $request->answers);
        $token = $request->user_token;
        $div = explode('-', $token);
        $test_id = $div[1];
        $test = Test::find($test_id);
        $evaluation = Evaluation::create([
            'token' => $div[0],
            'gender' => $request->gender,
            'birth_date' => $request->birth_date,
            'test_id'=>$test->id
        ]);

        foreach ($answers as $answer) {
            $tem = explode('-', $answer);
            $question = Question::find($tem[0]);

            $option = $question->options()->where('description', $tem[1])->first();

            $new_answer = Answer::create(['option_id' => $option->id, 'evaluation_id' => $evaluation->id, 'value' => $tem[1]]);


        }

        return $evaluation;
    }
}