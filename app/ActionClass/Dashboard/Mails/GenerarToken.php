<?php
namespace App\ActionClass\Dashboard\Mails;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;


class GenerarToken
{
    public static function execute()
    {
        $code;
        do {
            $code = Str::random(16);
            $params['code'] = $code;
            $validator = Validator::make($params, [
                'code' => ['unique:evaluations,token']
            ]);
        } while ($validator->fails());
        return $code;
    }
}