<?php

namespace App\ActionClass\Dashboard\Mails;

use Illuminate\Support\Facades\URL;
use App\Mail\SendMail;
use App\Mail\TestEmail;

class SendMailTest
{
    public static function execute($test, $lista)
    {
        $emails = $lista->emails_user;
        foreach ($emails as $email) {
            $token=GenerarToken::execute();
            $token=$token.'-'.$test->id;
            $url=URL::signedRoute('create_evaluation',['token' => $token]);
            \Mail::to($email->email_user)->queue(new TestEmail($test,$url));
        }

    }

}