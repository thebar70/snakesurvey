<?php

namespace App\ActionClass\Dashboard\Statistics;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Answer;

class ShowStatistics
{
    public static function execute()
    {
        $user = Auth()->user();
        $statis = array();//Varibale que contendra toda la informacion de estadisticas
        $options = array(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
        $questions = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,

        );
        //Obtencion de todos los test creados por el usuario
        $tests = $user->tests;
        $count = 0;
        foreach ($tests as $test) {
            if (!$test->evaluations->isEmpty()) {
                $cantidad_hombres = $test->evaluations()
                    ->where('evaluations.gender', 'man')
                    ->count();
                $cantidad_muejeres = $test->evaluations()
                    ->where('evaluations.gender', 'woman')
                    ->count();
                $total = $test->list_email->emails_user()->count();
                $restante = $total - $test->evaluations()->count();
                $info_test = array(
                    'pos' => $count,
                    'test_name' => $test->name,
                    'hombres' => $cantidad_hombres,
                    'mujeres' => $cantidad_muejeres,
                    'total' => $total,
                    'restante' => $restante,
                );

                $statis[] = $info_test;
                
                //Calificacion por opcion
                for ($i = 1; $i < 6; $i++) {
                    $dato = $test->evaluations()
                        ->join('answers', 'evaluations.id', 'answers.evaluation_id')
                        ->where('answers.value', $i)
                        ->count();
                    $options[$i - 1] += $dato;

                }
                //Calificaciones por pregunta
                
                foreach ($test->evaluations as $evaluation) {
                    foreach ($evaluation->answers as $answer) {
                        $dato = $answer->option->question->id;
                        $questions[$dato]++;
                    }
                }
                $count++;
            }
        }


        /* $answers = Answer::join('tests', 'answers.test_id', 'tests.id')
            ->where('tests.user_id', $user->id)
            ->get(); */


        return [$statis,$questions];
    }
}