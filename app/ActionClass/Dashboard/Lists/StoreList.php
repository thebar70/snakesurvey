<?php

namespace App\ActionClass\Dashboard\Lists;

use App\Result\Result;
use Illuminate\Http\Request;

use Symfony\Component\Console\Question\Question;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\ListEmail;
use App\Models\EmailUser;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;

class StoreList
{
    public static function execute(Request $data)
    {
        $user = Auth()->user();
        $lista = new ListEmail($data->all());
        $lista->user()->associate($user);
        $lista->save();
        
        if ($data->excel == null) {
            foreach ($data->mails as $email) {
                $new_email_user = new EmailUser(['email_user' => $email]);
                $new_email_user->list_email()->associate($lista);
                $new_email_user->save();
            }
        } else {
            Excel::import(new UsersImport($lista),$data->file('excel'));
        }

        return $lista;

    }
}