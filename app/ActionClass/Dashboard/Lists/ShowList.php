<?php

namespace App\ActionClass\Dashboard\Lists;

use Illuminate\Http\Request;

use Symfony\Component\Console\Question\Question;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ShowList
{
    public static function execute($id)
    {
        $user=Auth()->user();
        return $user->lists_email()->find($id);
    }
}