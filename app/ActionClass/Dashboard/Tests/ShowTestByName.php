<?php

namespace App\ActionClass\Dashboard\Tests;

use Illuminate\Http\Request;

use Symfony\Component\Console\Question\Question;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ShowTestByName
{
    public static function execute($words)
    {
        $user = Auth()->user();
        $tests = $user->tests()->select('tests.id','tests.name')
            ->where('name', 'like', '%' . $words . '%')
            ->get();

        return $tests;


    }
}