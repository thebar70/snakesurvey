<?php

namespace App\ActionClass\Dashboard\Tests;

use Illuminate\Http\Request;

use Symfony\Component\Console\Question\Question;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ShowTest
{
    public static function execute($id)
    {
        $user=Auth()->user();
        return $user->tests()->find($id);
        

    }
}