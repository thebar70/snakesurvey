<?php

namespace App\ActionClass\Dashboard\Tests;

use Illuminate\Http\Request;

use Symfony\Component\Console\Question\Question;
use App\Models\Test;
use Illuminate\Support\Facades\Auth;
use App\Models\ListEmail;
use App\Jobs\TestEmailJob;

class StoreTest
{
    public static function execute(Request $data)
    {
        $user=Auth()->user();
        $lista=ListEmail::find($data->list_id);
        $test = Test::create([
            'form_id' => $data->form_id,
            'site'=>$data->site,
            'name'=>$data->name,
            'user_id'=>$user->id,
            'list_email_id'=>$lista->id,
        ]);
        
        //Envio de email de notificacion
        TestEmailJob::dispatch($test,$lista);
        return $test;
    }
}