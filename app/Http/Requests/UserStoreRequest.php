<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>['bail','required','email','unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'names' => ['bail', 'required', 'string', 'max:255'],
            'birth_date' => ['bail','required','date'],
        ];
    }
}
