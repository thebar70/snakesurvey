<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->mails[0]){
            return [
                'name'=>['required','string','max:50'],
                'mails'=>['array'],
                'mails.*'=>['bail','email','unique:email_users,email_user'],
                'excel'=>['bail','required_without:mails']
            ];
        }
        return [
            'name'=>['required','string','max:50'],
            'excel'=>['bail','required_without:mails']
        ];
        
    }
}
