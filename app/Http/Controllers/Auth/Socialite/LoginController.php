<?php

namespace App\Http\Controllers\Auth\Socialite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use Exception;
use App\Models\User;
use App\Models\Role;



class LoginController extends Controller
{
    
    
    

    public function __construct()
    {
        
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback(Request $request)
    {
        try {
    
            $user = Socialite::driver('google')->user();
            
            $finduser = User::where('google_id', $user->id)->first();
            if($finduser){
            
                Auth::login($finduser);
                return redirect()->route('index_evaluator');
                
     
            }else{
                $newUser = User::create([
                    'names' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'avatar'=>$user->avatar_original,
                ]);
                $newUser->roles()->attach(Role::where('name', 'evaluator')->first());
                Auth::login($newUser);
     
                return redirect()->route('index_evaluator');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }

    }

    //Facebook
    public function handleFacebookCallback(Request $request)
    {
        try {
    
            $user = Socialite::driver('facebook')->user();
            $finduser = User::where('facebook_id', $user->id)->first();
            if($finduser){
            
                Auth::login($finduser);
                return redirect()->route('index_evaluator');
                
     
            }else{
                $newUser = User::create([
                    'names' => $user->name,
                    'email' => $user->email,
                    'facebook_id'=> $user->id,
                    'avatar'=>$user->avatar,
                ]);
                $newUser->roles()->attach(Role::where('name', 'evaluator')->first());
                Auth::login($newUser);
     
                return redirect()->route('index_evaluator');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }


    }

}
