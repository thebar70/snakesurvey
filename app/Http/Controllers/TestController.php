<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\ActionClass\Dashboard\Tests\StoreTest;
use App\Http\Requests\TestStoreRequest;
use App\ActionClass\Dashboard\Tests\ListTest;
use Illuminate\Support\Facades\Auth;
use App\ActionClass\Dashboard\Tests\ShowTest;
use App\ActionClass\Dashboard\Tests\ShowTestByName;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = ListTest::execute();
        return view('evaluator.listTests')
            ->with('tests', $tests)
            ->with('user', Auth()->user());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forms = Form::paginate(20);
        return view('evaluator.crearEncuesta')
            ->with('forms', $forms)
            ->with('user', Auth()->user());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestStoreRequest $request)
    {
        $test = StoreTest::execute($request);
        $resolved = 0;//Calcular veces resuelto
        flash('Se creó su encuesta correctamente')->success();
        return view('evaluator.showTest')
            ->with('test', $test)
            ->with('resolved', $resolved)
            ->with('user', Auth()->user());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test = ShowTest::execute($id);
        if ($test != null) {
            $resolved = 0;//Calcular veces resuelto
            flash('Información de encuesta')->success();
            return view('evaluator.showTest')
                ->with('test', $test)
                ->with('resolved', $resolved)
                ->with('user', Auth()->user());
        } else {
            flash('No se encontro el test especificado')->warning();
        }


    }

    public function show_by_name(Request $request)
    {
        $words=$request->get('words');
        $tests=ShowTestByName::execute($words);
        return json_encode($tests);
        
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
