<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Test;
use App\ActionClass\Users\Evaluations\StoreEvaluation;
use App\Models\Evaluation;

class EvaluationController extends Controller
{
    public function resolve(Request $request)
    {
        dd($request);
        /* $statistics=ShowStatistics::execute();
        return view('evaluator.showStatistics')
            ->with('user', Auth()->user())
            ->with('statistics', $statistics); */
    }
    public function create(Request $request)
    {
        $req_token = $request->token;
        $div = explode('-', $req_token);
        $test_id = $div[1];
        $token = $div[0];
        if (!Evaluation::where('token', $token)->first()) {
            $test = Test::find($test_id);
            return view('users.showEvaluation')
                ->with('test', $test)
                ->with('user_token', $request->token);
        }
        return view('users.ya');
                

    }
    public function store(Request $request)
    {
        $evaluation = StoreEvaluation::execute($request);
        return view('users.thanksUser');
       
    }
}
