<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ListStoreRequest;
use App\ActionClass\Dashboard\Lists\StoreList;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('evaluator.createList')
            ->with('user', Auth()->user());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListStoreRequest $request)
    {
        $user= Auth()->user();
        $list = StoreList::execute($request);
        $lists= $user->lists_email;
        flash('Su lista fue creada correctamente')->success();
        return redirect()->route('create_form');
        /* return view('evaluator.showList')
            ->with('user', Auth()->user())
            ->with('list', $list)
            ->with('lists', $lists);  */       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= Auth()->user();
        $list = $user->lists_email->find($id);
        $lists= $user->lists_email;
        return view('evaluator.showList')
            ->with('user', Auth()->user())
            ->with('list', $list)
            ->with('lists', $lists);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function obtenerLista()
    {
        return array(1,2,3);
    }
}
