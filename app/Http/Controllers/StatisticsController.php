<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ActionClass\Dashboard\Statistics\ShowStatistics;

class StatisticsController extends Controller
{
    public function show()
    {
        $res=ShowStatistics::execute();
        
        return view('evaluator.showStatistics')
            ->with('user', Auth()->user())
            ->with('statistics', $res[0])
            ->with('questions', $res[1]);
    }
}
