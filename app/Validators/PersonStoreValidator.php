<?php
namespace App\Validators;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Result\Result;

class PersonStoreValidator
{
    public static function validate(Request $request)
    {
        $result;
        $validator = Validator::make($request->all(), [
            'email'=>['bail','required','email','unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'names' => ['bail', 'required', 'string', 'max:255'],
            'surnames' => ['bail', 'required', 'string', 'max:255'],
            'birth_date' => ['bail','required','date'],
        ]);
        if ($validator->fails()) {
            $result = new Result(470, $validator->errors()->all());
        } else {
            $result = new Result(200, 'accept');
        }
        return $result;
    }
}
