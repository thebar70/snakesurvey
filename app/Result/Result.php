<?php
namespace App\Result;

class Result
{
    protected $code;
    protected $response;

    
    public function __construct()
    {
        
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
        
    }
    public function __construct2($code, $response)
    {
        $this->code = $code;
        $this->response = $response;
    }
    public function set_code($code){
        $this->code=$code;
    }
    public function set_response($response){
        $this->response=$response;
    }
    public function get_response(){
        return $this->response;
    }
    public function get_code(){
        return $this->code;
    }


}
