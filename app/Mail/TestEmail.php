<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;
    private $test;
    private $url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($test,$url)
    {
        $this->test=$test;
        $this->url=$url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Nueva Encuesta')
            ->view('email.testEmail')
            ->with('test', $this->test)
            ->with('url',$this->url);
    }
}
