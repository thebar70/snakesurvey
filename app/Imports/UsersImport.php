<?php

namespace App\Imports;

use App\Models\EmailUser;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class UsersImport implements ToModel, WithValidation
{
    use Importable;

    private $lista;

    public function __construct($lista)
    {
        $this->lista=$lista;
    }
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $new_email_user = new EmailUser([
            'email_user' => $row[0]
        ]);
        $new_email_user->list_email()->associate($this->lista);
        $new_email_user->save();
        return $new_email_user;
    }

    public function rules(): array
    {
        return [
            '0'=>'email|unique:email_users,email_user',
        ];
    }
}
