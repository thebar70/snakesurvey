<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Jhonatan Montilla">
    <meta name="description" content="Dasboar evaluador">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Mis Encuestas</title>

    
    <!-- Bootstrap Css -->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <script src="{{asset('asset/bootstrap/js/core/jquery.3.2.1.min.js')}}"></script>
    <!---->

    <!-- Hoja de estilos -->
    <link href="{{asset('asset/css/styles3.css')}}" rel="stylesheet">
    

    <!-- Google fonts -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Muli:400,700&display=swap')}}" rel="stylesheet">

    <!-- Ionic icons -->
    <link href="{{asset('https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css')}}" rel="stylesheet">

    

</head>

<body>

    <div class="d-flex" id="content-wrapper">

        <!-- Sidebar -->
        <div id="sidebar-container" class="bg-dark bg-light border-right">
            <div class="logo">
                <div class="text">
                <a class="navbar-brand" href="#"> <img src={{asset("asset/images/Logo3Blanco.png")}} class="logoSnake"
                        alt="logo"></a>
                </div>
            </div>
            <div class="menu list-group-flush">
                <a href="{{route('create_form')}}"
                    class="list-group-item list-group-item-action text-muted p-3 border-0"><i
                        class="icon ion-md-add lead mr-2"></i> Crear Encuestas</a>
                <a href="{{route('list_tests')}}"
                    class="list-group-item list-group-item-action text-muted bg-white p-3 border-0"><i
                        class="icon ion-md-browsers lead mr-2"></i> Mis Encuestas</a>
                <a href="{{route('create_list')}}"
                    class="list-group-item list-group-item-action text-muted bg-white p-3 border-0"><i
                        class="icon ion-md-people lead mr-2"></i> Crear Lista de Usuarios </a>
                
                <a href="{{route('show_statistics')}}" class="list-group-item list-group-item-action text-muted bg-white p-3 border-0"><i
                        class="icon ion-md-stats lead mr-2"></i> Estadísticas </a>
            </div>
        </div>
        <!-- Fin sidebar -->

        <!-- Page Content -->
        <div id="page-content-wrapper" class="w-100 bg-light-blue">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <div class="container">
                    <!-- boton de menu -->
                    <a href="#" id="menu-toggle"> <img src={{asset("asset/images/menu1.png")}}></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- Fin -->
                    <!-- barra de busqueda -->
                    <div class="col-md-6">
                                
                            <input type="text" size="30" onkeyup="showResult(this.value)" placeholder="Buscas una encuesta?">
                            <div id="livesearch">
                                    
                            </div>
                        
                    </div>
                    <!-- Fin -->
                    <div class="collapse navbar-collapse"id ="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item toggle-nav-search hidden-caret">
                                <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button"
                                    aria-expanded="false" aria-controls="search-nav">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                            <!-- Cerrar Sesion -->
                            <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"
                                    aria-expanded="false">
                                    <div class="avatar-sm">
                                        @if ($user->avatar!=null)
                                        <img src="{{$user->avatar}}" alt="..." class="logoAvatar rounded-circle" class="avatar-img rounded-circle">
                                        @else
                                        <img src="{{asset('asset/images/usuario.png')}}" alt="..."
                                           class="logoAvatar rounded-circle" class="avatar-img rounded-circle">
                                        @endif

                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-user animated fadeIn">
                                    <div class="dropdown-user-scroll scrollbar-outer">
                                        <li>
                                            <div class="user-box">
                                                @if ($user->avatar!=null)
                                                <div class="avatar-lg"><img src="{{$user->avatar}}" class="logoAvatar rounded-circle" alt="image profile"
                                                        class="avatar-img rounded"></div>
                                                @else
                                                <div class="avatar-lg"><img src="{{asset('asset/images/usuario.png')}}" class="logoAvatar"
                                                        alt="image profile" class="avatar-img rounded"></div>
                                                @endif

                                                <div class="u-text">
                                                    <h4>{{$user->name}}</h4>
                                                    <p class="text-muted">{{$user->email}}</p><a href="profile.html"
                                                        class="btn btn-xs btn-secondary btn-sm">View Profile</a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">My Profile</a>
                                            <a class="dropdown-item" href="#">My Balance</a>
                                            <a class="dropdown-item" href="#">Inbox</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Account Setting</a>
                                            <div class="dropdown-divider"></div>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                <button type="submit" class="btn-ghost tertiary round">
                                                    {{ __('Logout') }}
                                                </button>
                                                @csrf
                                            </form>

                                        </li>
                                    </div>
                                </ul>
                            </li>
                            <!-- Fin -->
                        </ul>
                    </div>
                </div> <!-- Fin Container -->
            </nav>
            <div class="main-panel">
                <div class="content">
                    @include('flash::message')
                    @yield('content')
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Page Content -->
    </div>
    <!-- Fin wrapper -->

    <!-- Bootstrap y JQuery -->
    <script src="{{asset('asset/js/jquery.js')}}"></script>
    <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>

    <!-- Abrir / cerrar menu -->
    
    <script>
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#content-wrapper").toggleClass("toggled");
        });
    </script> 
    <script>
            
            function showResult(str) {
              if (str.length==0) {
                document.getElementById("livesearch").innerHTML="";
                document.getElementById("livesearch").style.border="0px";
                return;
              }
              $.ajax({
                    type:'get',
                    url:'https://snakesurvey.venoudev.com/dashboard/test/show_tests_by_name',
                    data:{'words':str},
                    success:function(data){
                        var json=JSON.parse(data);
                        console.log(json);
                        json.forEach(element =>{
                            document.getElementById("livesearch").innerHTML=element.name;
                            document.getElementById("livesearch").innerHTML=element.name;
                                  
                        });
                        document.getElementById("livesearch").style.border="2px solid #A5ACB2";        
                            
                            
                        
                    }
              });
              
            }
            
            </script>   
    <script>
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

</body>

</html>