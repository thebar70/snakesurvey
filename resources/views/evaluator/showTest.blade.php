@extends('evaluator.navEvaluator')

@section('content')

<div id="content" class="container-fluid p-5">
  <section class="py-3">
    <!-- Highlights -->
    <div class="row">
            <div class="col-6">
                <!-- Contenedor Información Test-->
                <br>
                <br>
                
                <div class="modal-content">
                    
                    <div class="text-center">
                        <h3>Información de la Encuesta</h3>
                        <form action="">
                            <br>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nombre Del Test</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Username" placeholder="{{$test->name}}"
                                    aria-describedby="basic-addon1" readonly>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Fecha de Creación</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Username" placeholder="{{$test->date}}"
                                    aria-describedby="basic-addon1" readonly>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Username" placeholder="{{$test->form->name}}"
                                    aria-describedby="basic-addon1" readonly>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                <!-- Contenedor Preguntas-->
                <div class="modal-content">
                    <div class="text">
                        <h3><br>  PREGUNTAS</h3>
                        <ol class="activity-feed">
                            @foreach ($test->form->questions as $question)
                                <li class="feed-item feed-item-secondary">
                                    <span class="text">
                                        {{$question->description}}
                                    </span>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            </div>

            <div class="col-6">
                <!-- Contenedor de la URL -->
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">URL del Sitio</span> 
                    </div>
                    <input type="text" class="form-control" aria-label="Username" placeholder="{{$test->site}}"
                        aria-describedby="basic-addon1" readonly>
                </div>
                
            </div>
        </section>
    </div>
@endsection