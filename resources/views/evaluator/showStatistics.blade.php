@extends('evaluator.navEvaluator')
{{-- <script src="{{asset('asset/js/chart.min.js')}}"></script> --}}
<script src="{{asset('asset/js/circles.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
@section('content')

<div id="content" class="container-fluid p-5">
    <section class="py-6">
      <!-- Highlights -->
        
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row card-tools-still-right">
                                <h4 class="card-title">Estadisticas por Test</h4>
                            </div>
                            <p class="card-category">
                            Seleccione un test u opción</p>
                            <select class="form-control form-control-sm-3 col-md-6" name="gender" id="gender" required>
                                    <option value="" id="start">Seleccione una opcion...</option>
                                    @foreach ($statistics as $statistic)
                                        <option value="{{$statistic['pos']}}">{{$statistic['test_name']}}</option>
                                    
                                    @endforeach
                                    
                            </select>
                            <label for="">Si su test no esta presente, es por que aun no ha sido resuelto</label>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="card-title">Informacion por genero</div>
                                <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                    <div class="px-2 pb-2 pb-md-0 text-center">
                                        <div id="circles-1" value></div>
                                        <h6 class="fw-bold mt-3 mb-0">Resuletas por hombres</h6>
                                    </div>
                                    <div class="px-2 pb-2 pb-md-0 text-center">
                                        <div id="circles-2"></div>
                                        <h6 class="fw-bold mt-3 mb-0">Resuletas por mujeres</h6>
                                    </div>
                                    <div class="px-2 pb-2 pb-md-0 text-center">
                                        <div id="circles-3"></div>
                                        <h6 class="fw-bold mt-3 mb-0">Sin resolver</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Respuestas por genero</div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container">
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    <br><br><br>
</div>
<script>
    const statistics = @json($statistics);
    const questions = @json($questions);
    $('select').on('change', function() {
        if (this.id != 'start') {
            /* Inicio de line chart */
            console.log(questions);
            var lineChart = document.getElementById('lineChart').getContext('2d');
            var myLineChart = new Chart(lineChart, {
                type: 'line',
                data: {
                    labels: ['p1','p2','p3','p4','p5','p6','p7','p8','p9','p10'],
                    datasets: [{
                        label: "Edad",
                        borderColor: "#1d7af3",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#1d7af3",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        backgroundColor: 'transparent',
                        fill: true,
                        borderWidth: 2,
                        data: [questions[1],questions[2],
                        questions[3],
                        questions[4],
                        questions[5],
                        questions[6],
                        questions[7],
                        questions[8],
                        questions[9],
                        questions[10],
                    ]
                    }]
                },
                options : {
                    responsive: true, 
                    maintainAspectRatio: false,
                    legend: {
                        position: 'bottom',
                        labels : {
                            padding: 10,
                            fontColor: '#1d7af3',
                        }
                    },
                    tooltips: {
                        bodySpacing: 4,
                        mode:"nearest",
                        intersect: 0,
                        position:"nearest",
                        xPadding:10,
                        yPadding:10,
                        caretPadding:10
                    },
                    layout:{
                        padding:{left:15,right:15,top:15,bottom:15}
                    }
                }
            });
            /* Fin Line Chart */
            /* Inicio de circulos */
            
            Circles.create({
                id:'circles-1',
                radius:45,
                value:statistics[this.value]['hombres'],
                maxValue:statistics[this.value]['total'],
                width:7,
                text: statistics[this.value]['hombres'],
                colors:['#f1f1f1', '#0087fc'],
                duration:400,
                wrpClass:'circles-wrp',
                textClass:'circles-text',
                styleWrapper:true,
                styleText:true
            })

            Circles.create({
                id:'circles-2',
                radius:45,
                value:statistics[this.value]['muejeres'],
                maxValue:statistics[this.value]['total'],
                width:7,
                text: statistics[this.value]['mujeres'],
                colors:['#f1f1f1', '#2BB930'],
                duration:400,
                wrpClass:'circles-wrp',
                textClass:'circles-text',
                styleWrapper:true,
                styleText:true
            })

            Circles.create({
                id:'circles-3',
                radius:45,
                value:statistics[this.value]['restante'],
                maxValue:statistics[this.value]['total'],
                width:7,
                text: statistics[this.value]['restante'],
                colors:['#f1f1f1', '#fc0000'],
                duration:400,
                wrpClass:'circles-wrp',
                textClass:'circles-text',
                styleWrapper:true,
                styleText:true
            })
            /* Find de circulos */

        }
    });
</script>
    
@endsection
