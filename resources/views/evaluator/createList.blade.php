@extends('evaluator.navEvaluator')
@section('content')
<div id="content" class="container-fluid p-5">
  <section class="py-3">
    <!-- Highlights -->
    <div class="row">
      <form class="" action="{{route('store_list')}}" method="POST" id="form_x" enctype="multipart/form-data"
          data-parsley-validate class="form-horizontal form-label-left">
          {{ csrf_field() }}
          <div class="page-inner mt--5">
            <div class="row mt--5">
              <div class="col-mr-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="strong" style="color: black;">Nueva lista de usuarios
                      </h2>
                    </div>
                  </div>
                  <div class="card-body">

                    <div class="row">
                          <div class="form-group col-md-6 col-lg-4 col-xs-12">
                            <label for="email2">Nombre</label>
                            <input type="text" required="required"
                              class="form-control number @error('name') is-invalid @enderror" id="name"
                              value="{{ old('name') }}" name="name" placeholder="Nombre de la lista">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                          </div>
                          <div class="form-group col-md-6 col-lg-4 col-xs-12">
                            <label for="email2">O prefiere cargar un excel?</label>
                            
                                <input type="file" id="excel" name="excel" class="form-control number @error('excel') is-invalid @enderror">
                                @error('excel')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            
                          </div>
                          {{-- Aqui Agregar autmatico --}}
                          <div class="container">
                            <div class="row clearfix">
                              <div class="col-md-12 column">
                                <table class="table table-bordered" id="tab_logic">
                                  <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">
                                        #
                                      </th>
                                      <th scope="col">
                                        Mail
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr id='addr0'>
                                      <td>1</td>
                                      <td>
                                        <input type="email" name='mails[]' placeholder='Email Usuario'
                                          class="form-control @error('mails.*') is-invalid @enderror" />
                                        @error('mails.*')
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                      </td>
                                    </tr>
                                    <tr id='addr1'></tr>
                                  </tbody>

                                </table>
                              </div>
                            </div>
                            <input type="button" class="btn btn-success pull-left" id="add_row" value="+" />
                            <input type="button" class="pull-right btn btn-success" id='delete_row' value="-"></button>
                          </div>
                      {{-- Hasta Aqui --}}
                    </div>
                      <div class="text text-center form-group col-md-4 col-lg-12 col-xs-12">
                        <button class="btn btn-dark">Guardar</button>
                      </div>
                      <br><br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
    </div>
  </section>
</div>

<script type="">

  $(document).ready(function () {
    var i = 1;
    $("#add_row").click(function () {
      b = i - 1;
      $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
      $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
      i++;
    });
    $("#delete_row").click(function () {
      if (i > 1) {
        $("#addr" + (i - 1)).html('');
        i--;
      }
    });

  });
</script>

@endsection