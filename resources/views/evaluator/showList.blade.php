@extends('evaluator.navEvaluator')

@section('content')
<br><br><br><br>
    {{ csrf_field() }}
     <div class="page-inner mt--5">
      <div class="row mt--2">
         <div class="col-md-12">
           <div class="card">
             <div class="card-header">
               <div class="card-title">
                 <h3 class="strong">Listas de correos de usuarios:
                 </h3>
               </div>
             </div>
             <div class="card-body">

                <div class="row">
                    <div class="form-group col-md-6 col-lg-6 col-xs-12">
                        <label for="email2">Mis listas</label>
                        <select id="list1_id" class="form-control number @error('list1_id') is-invalid @enderror" name="list1_id" value="{{ old('list1_id') }}">
                           @foreach($lists as $list1)
                                
                               <option name="" {{ action('ListController@show', ['id'=>1]) }}
                                   @if ($list!=null)
                                      @if ($list->id==$list1->id)
                                        selected="true" 
                                      @endif
                                  @endif 
                                  value="{{$list1->id}}">{{$list1->name}}
                                  
                              </option>
                           @endforeach
                           @error('list1_id')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </select> 
                      </div>
                   <!-- Aqui va el contenido-->
                   <div class="form-group col-md-12 col-lg-6 col-xs-12">
                    <div class="table-responsive">
                      <table id="add-row" class="display table  table-sm table-striped table-hover table-bordered" >
                        <thead>
                          <tr>
                            <th>Email</th>
                            <th style="width: 8%">Acciones</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($list->emails_user as $email)
                            <tr>
                              <td>{{$email->email_user}}</td>
                              {{-- Acciones --}}
                              <td>
                                  <div class="form-button-action">
                                      <button type="button" onclick="location.href='{{route('update_email_list',$email->id)}}'"  data-toggle="tooltip" id="editar" title="" class="btn btn-link  btn-primary" data-original-title="Editar">
                                          <i class="fa fa-edit"></i>
                                      </button>
                                  </div>
                                  <div class="form-button-action">
                                      <form action="{{route('delete_email_list')}}" onSubmit="eliminar(this.id)" id="eliminar{{$email->id}}" method="POST" id="form_x" data-parsley-validate class="form-horizontal form-label-left">
                                      {{method_field('DELETE')}}
                                      {{ csrf_field() }}
                                          <input type="hidden" readonly id="eliminar{{$email->id}}" value="{{$email->id}}" name="id" required="required">

                                      <button type="submit" data-toggle="tooltip" id="confirm_delete" title="" class="btn btn-link  btn-danger" data-original-title="Eliminar">
                                          <i class="fa fa-trash"></i></button>
                                      </form>
                                  </div>
                              </td>
                              {{-- Hasta Aqui Acciones --}}
                              
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      <div class="text-center justify-content-center">

                        {{-- {{ $email->links('pagination::bootstrap-4') }} --}}
                    </div>
                    </div>
                  </div>
             </div>
           </div>
           
         </div>
       </div>
     </div>
   </div>
<script >
 $(document).ready(function(){
      /* var i=1;
     $("#add_row").click(function(){b=i-1;
      $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
    });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 }
	 }); */
   $("#list1_id").click(function(){
      var selectBox = document.getElementById("list1_id");
      var selectedValue = selectBox.options[selectBox.selectedIndex].value;

  });

});
</script>

@endsection