@extends('evaluator.navEvaluator')
@section('content')

<div id="content" class="container-fluid p-5">
  <section class="py-3">
    <!-- Highlights -->
				 <div class="card">
					 <div class="card-header">
						 <div class="card-title">
							 <h3 class="strong" style="color: black;">Mis encuestas
							 </h3>
						 </div>
					 </div>
					 <div class="card-body">
						 <!-- Contenedor Preguntas-->
								<div class="text">										
										<ol class="activity-feed">
												@foreach ($tests as $test)
														<li class="feed-item feed-item-secondary">
																<span class="text">
																		{{$test->name}} {{"   "}} {{$test->site}}
																</span>
																<button type="btn btn-icon btn-round btn-success" onclick="location.href='{{route('show_test',$test)}}';"  data-toggle="tooltip" id="id" title="" class="btn btn-link  btn-primary" data-original-title="Ver">
																		Detalles
																</button>
														</li>
														{{-- <li class="feed-item feed-item-secondary">

																<span class="text">{{$visita->cliente->persona->nombre."-".$visita->cliente->persona->direccion}} </span>
																@if ($visita->estado!='visitado')
																	<button type="btn btn-icon btn-round btn-success" onclick="location.href='{{route('marcar_visita',$visita)}}';"  data-toggle="tooltip" id="marcar" title="" class="btn btn-link  btn-primary" data-original-title="Visitado">
																			<i class="fa fa-edit"></i>
																		</button>
																@endif


														</li> --}}
												@endforeach
										</ol>
								</div>
						<br><br><br><br><br><br>
						{{-- Fin contener de preguntas --}}
					</div>
				</div>
				<br><br><br><br><br><br>
	</section>
</div>

@endsection

