@extends('evaluator.navEvaluator')
@section('content')
<section>
  <div id="content" class="container-fluid p-5">
    <section class="py-3">
      <!-- Highlights -->
      <div class="row">
        <form class="" action="{{route('store_test')}}" method="POST" id="form_x" data-parsley-validate
          class="form-horizontal form-label-left">
          {{ csrf_field() }}
          <div class="page-inner mt--5">
            <div class="row mt--5">
              <div class="col-mr-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="strong" style="color: black;">Creación de la Encuesta
                      </h2>
                    </div>
                  </div>
                  <div class="card-body">

                    <div class="row">

                      <div class="form-group col-md-6 col-lg-6 col-xs-12">
                        <label for="email2">Nombre</label>
                        <input type="text" required="required"
                          class="form-control number @error('site') is-invalid @enderror" id="name"
                          value="{{ old('name') }}" name="name" placeholder="Nombre de la encuesta">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>

                      <div class="form-group col-md-6 col-lg-4 col-xs-12">
                        <label for="email2">URL</label>
                        <input type="text" class="form-control number @error('site') is-invalid @enderror"
                          required="required" value="{{ old('site') }}" name="site" placeholder="URL del sitio">
                        @error('site')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>
                      <div class="form-group col-md-6 col-lg-6 col-xs-12">
                        <label for="email2">Tipo</label>
                        <select id="form_id" class="form-control number @error('form_id') is-invalid @enderror"
                          name="form_id" value="{{ old('form_id') }}
                       onchange=" mostrar(this.value);">
                          <option value="">Tipo de encuesta</option>
                          @foreach($forms as $form)
                          <option name="" value="{{$form->id}}">{{$form->name}}</option>
                          @endforeach
                          @error('form_id')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                        </select>
                      </div>
                      <div class="form-group col-md-6 col-lg-6 col-xs-12">
                        <label for="email2">Emails</label>
                        <select id="list_id" class="form-control number @error('list_id') is-invalid @enderror"
                          name="list_id" value="{{ old('list_id') }}">
                          <option value="">Seleccione una lista</option>
                          @foreach($user->lists_email as $list_email)
                          <option name="" value="{{$list_email->id}}">{{$list_email->name}}</option>
                          @endforeach
                          @error('list_id')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                        </select>
                        <a class="nav-link js-scroll-trigger" href="{{route('create_list')}}">
                          Nueva lista de usuarios
                        </a>

                      </div>
                    </div>

                  </div>
                  <div class="card-action">
                    <div class="text text-center form-group col-md-6 col-lg-6 col-xs-12">
                      <button class="btn btn-success">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    //Para cuando se de click sobre crear usuario
    $('#list_email').hide();
    $(document).ready(function () {
      $("#list_id").click(function () {
        var e = document.getElementById('list_id');
        var selected = e.options[e.selectedIndex].value;
        if (selected == 'new_list') {
          $('#list_email').show();
        }


      });
    });
  </script>
@endsection