<footer class="top-padding bg-dark">
        <!--Content -->
        <div class="container">
            <div class="row align-self-center">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <!-- Brand -->
                        <a href="#" class="footer-brand text-white">
                            Ronald Solutions
                        </a>
                        <p> Una empresa que se encuentra a tu servicio.</p>
                    </div>
                </div>
    
            </div>
            <!-- / .row -->
    
            <div class="row justify-content-md-center footer-copy">
                <div class="col-lg-8 col-md-6 col-sm-6 text-center">
                    <p class="lead text-white-50">&copy; Copyright Reservado a Ronald Solutions | Diseñado y Desarollado por <a href="https://venoudev.com">VenouDev</a> Soluciones Astutas y Robustas</p>
                </div>
            </div>
        </div>
        <!-- / .container -->
    </footer>