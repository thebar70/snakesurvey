<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- Custom Styles -->
  <link rel="stylesheet" href="{{asset('asset/css/styles2.css')}}">

  <!-- Google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300&display=swap" rel="stylesheet">

  <!-- Ionic icons -->
  <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">

  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
    crossorigin="anonymous" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"
    crossorigin="anonymous"></script>

  

  <title>Evaluator</title>
</head>

<body>

  <nav class="navbar navbar-expand-lg position-absolute w-100 bg-transparent z-top navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand text-light" href="#"><b>Snake Survey</b></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a id="circle" class="nav-link d-flex position-relative" href="#">
              <i id="menu-open" class="icon ion-md-menu position-absolute lead text-light" onclick="openMenu()"></i>
              <i id="menu-close" class="icon ion-md-close position-absolute lead text-light" onclick="hideMenu()"></i>
            </a>
          </li>
        </ul>
      </div>

      <!-- Log out-->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
              @csrf
          </form>
          </div>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container">
    <div class="row justify-content-center align-items-center minh-100">
      <div class="col-lg-12">
        <div>
          <img class="img-fluid rounded mx-auto d-block" src={{asset("asset/images/Logo3.png")}} alt="logo" width="25%" height="25%">
        </div>

        <div>
        </div>
      </div>

    </div>
  </div>


  <div id="menu" class="position-absolute w-100">
    <div class="row no-gutters">
      <div class="col-lg-6">
        <div id="menu-dark" class="bg-dark">
          <div id="content-dark" class="d-flex flex-column">
            <ul class="list-unstyled display-4 mb-5">
              <li class="mb-3"><a href="" class="text-light">Home</a></li>
              <li class="mb-3"><a href="{{route('create_form')}}" class="text-light">Crear Encuesta</a></li>
              <li class="mb-3"><a href="" class="text-light">Listado de Encuestas</a></li> 
            </ul>
            <ul class="list-unstyled text-light">
              <li>admin@snakesurvey.com</li>
              <li>tel. 55 9882 2829</li>
            </ul>
            <ul class="list-inline">
              <li class="list-inline-item"><a href=""><i class="icon ion-logo-facebook lead text-light mr-3"></i></a>
              </li>
              <li class="list-inline-item"><a href=""><i class="icon ion-logo-linkedin lead text-light mr-3"></i></a>
              </li>
              <li class="list-inline-item"><a href=""><i class="icon ion-logo-instagram lead text-light"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div id="menu-light" class="bg-light">
          <div id="content-light">
            <a href="#">
              <img src={{asset("asset/images/img-02.jpg")}} class="img-fluid mb-3">
            </a>
            <h5 class="mb-0">Snake Survey, your ideal survey</h5>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
        crossorigin="anonymous"></script>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  
  <script>
    function openMenu() {
      let menuDark = document.getElementById('menu-dark').style.width = '100%';
      let menuLight = document.getElementById('menu-light').style.width = '100%';
      let circle = document.getElementById('circle').style.cssText = 'background-color: #000000; box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)';
      let menuOpen = document.getElementById('menu-open').style.cssText = 'transform: rotate(90deg); opacity: 0';

      setTimeout(() => {
        let menuClose = document.getElementById('menu-close').style.cssText = "opacity: 100; z-index: 9999";
      }, 500)

      setTimeout(() => {
        let contentDark = document.getElementById('content-dark').style.opacity = '1';
        let contentLight = document.getElementById('content-light').style.opacity = '1';
      }, 1000)
    }

    function hideMenu() {
      let menuClose = document.getElementById('menu-close').style.cssText = "opacity: 0; z-index: 999";
      let circle = document.getElementById('circle').style.cssText = 'background-color: transparent; box-shadow: none';

      let contentDark = document.getElementById('content-dark').style.opacity = '0';
      let contentLight = document.getElementById('content-light').style.opacity = '0';

      setTimeout(() => {
        let menuOpen = document.getElementById('menu-open').style.cssText = 'transform: rotate(0deg); opacity: 1';
      }, 500)

      setTimeout(() => {
        let menuDark = document.getElementById('menu-dark').style.width = '0';
        let menuLight = document.getElementById('menu-light').style.width = '0';
      }, 1000)
    }
  </script>
</body>

</html>