<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <!-- Conectando con la hoja de estilos Css -->
  <link href="{{asset('asset/css/estilos.css')}}" rel="stylesheet">

  <!-- Google Fonds -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600&display=swap" rel="stylesheet">

  <!-- Ionicons -->
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
  <link href="{{asset('https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css')}}" rel="stylesheet">

  <title>Inicio de Sesión</title>
</head>
<!-- Begin Page Content -->
<div class="modal-dialog text-center">
    <br><br>
    <!-- Utiliza toda la pantalla-->
    <div class="col-sm-12">
      <div class="modal-content">
      <a class="navbar-brand" href="{{route('index')}}"> <img src={{asset("asset/images/Logo3.png")}}         class="logoSnake"  alt="logo"></a>
        <div>
          <br>
          
          <h8><b>Para continuar, inicia sesión en Snake Survey</b>
            <a href="{{route('login_google')}}" class="btn-ghost tertiary round" >CONTINUAR CON GOOGLE  <i2 class="icon ion-logo-google" ></i2></a>
          
          <br>
          <br>
            <a href="{{route('login_facebook')}}" class="btn-ghost round">CONTINUAR CON FACEBOOK  <i class="icon ion-logo-facebook"></i></a>
          <br>
          <h6 class="efectoLinea"><b>o</b></h6>
        </div>
        <!-- Aquí empiezan los campos de la creación de usuario -->
        <form method="POST" action="{{route('login')}}">
        <!-- Campo de Email o nombre de usuario -->
          @csrf
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" 
              required autocomplete="email" autofocus placeholder="Correo electrónico o nombre de usuario">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            <br>                            
            
          <!-- Campo de password -->
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                 name="password" required autocomplete="current-password" placeholder="Password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <br>
            <!-- Boton Iniciar Sesión -->
            <button type="submit" class="btn-ghost secundary round">
                   {{ __('INICIAR SESIÓN') }}
            </button>
            <br>
            @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('¿Has olvidado tu contraseña?') }}
                    </a>
            @endif
                            
        </form>
        <h5><b>¿No tienes cuenta?</b></h5><br>
        <a href="{{route('register_person')}}" class="btn-ghost tertiary round">
            {{ __('Registrate en Snake Survey') }}
        </a>
        <br>
        
      </div>
    </div>
</div>

</html>