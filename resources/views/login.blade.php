<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}">
  {{-- <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/atlantis.css')}}"> --}}

  <!-- Conectando con la hoja de estilos Css -->
  <link rel="stylesheet" href="{{asset('asset/css/estilos.css')}}">

  <!-- Google Fonds -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600&display=swap" rel="stylesheet">

  <!-- Ionicons -->
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

  <title>Inicio de Sesión</title>
</head>
<body>
<div class="modal-dialog text-center">
  <img src="images/LogoSnakeSurveyNegro2.png" alt="">
    <!-- Utiliza toda la pantalla-->
    <div class="col-sm-12">
      <div class="modal-content">
        <div>
          <br>
          <h5><b>Para continuar, inicia sesión en Snake Survey</b>
          <a href="{{route('login_google')}}" class="btn-ghost round">CONTINUAR CON FACEBOOK</a>
          <br>
          <h6 class="efectoLinea"><b>o</b></h6>
        </div>
        <!-- Aquí empiezan los campos de la creación de usuario -->
        <form action="{{route('login')}}">
        <!-- Campo de E-mail-->
        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
          </div>
          <!-- Campo de E-mail-->
          <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
            </div>
          <!-- Boton 'Recordarme' -->
          <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
          </div>
          <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
        </form>
        <h5><b>¿No tienes cuenta?</b></h5><br>
        <button class="btn-ghos tertiary round" >Registrate en Snake Survey</button>
      </div>
    </div>
</div>

</body>
</html>