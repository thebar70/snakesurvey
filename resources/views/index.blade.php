<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}">
    {{-- <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/atlantis.css')}}"> --}}

    <!-- Conectando con la hoja de estilos Css -->
    <link href="{{asset('asset/css/estilos.css')}}" rel="stylesheet">

    <!-- Google Fonds -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600&display=swap" rel="stylesheet">

    <!-- Ionicons -->
    <link href="{{asset('https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css')}}" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <style>
        html{
            scroll-behavior:smooth;
        }
    </style>

    <title>Snake Survey</title>}
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="{{route('index')}}"> <img src={{asset("asset/images/Logo3.png")}} class="logoSnake"
                    alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <ion-icon name="menu-outline"></ion-icon>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('index')}}" id="Inicio">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('register_person')}}" id="Registrase"><b>Registrarse</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}" id="Iniciar Sesión">Iniciar Sesion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact" id="Contacto">Contacto</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Sección de Hero-->
    <section id="hero">
        <div class="container">
            <div class="content-center">
                <br><br><br>
                <h1>¡Crea encuestas para los sitios web más populares!</h1>
                <p>Conéctese con personas por correo electrónico. ¡Con nuestras plantillas expertas y preescritas, puede
                    crear encuestas en cuestión de minutos!
                </p>
                <a href="#portafolio" class="btn btn-light"> <b>Explora</b>
                    <ion-icon name="caret-down-circle-outline"></ion-icon>
                </a>
            </div>
        </div>
    </section>

    <!-- Sección de Portafolio-->
    <br><br>
    <section id="portafolio">
        <div class="container-fluid">
            <div class="content-center">
                <h2>Realizamos encuestas de sus sitios <b>web favoritos</b></h2>
                <p>Las encuestas brindan información procesable y nuevas ideas. Estamos especializados en crear encuestas predefinidas para sitios web, ofrecemos una amplia gama de herramientas para crear su encuesta. <br> <b>¿Qué estás esperando para crear tu encuesta?</b></p>                
            </div>

            <!--Filas-->
            <div class="row">
                <!--Número de filas-->
                <div class="col-md-6">
                    <div class="portafolio-container">
                        <img src={{asset("asset/images/MiPortafolio-01.jpg")}} class="img-fluid" alt="portafolio 1">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portafolio-container">
                        <div class="text-center">
                            <br><br><br><br><br><br>
                            <h2>Obtenga estadísticas de sus usuarios</h2>
                            Cuantifique su realidad y disponga de elementos que le ayuden a tener otro punto de vista para así permitir un mejor análisis.
                            <p></p>
                        </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="portafolio-container">
                        <div class="text-center">
                            <br><br><br><br><br><br>
                            <h2>¡Crea encuestas en solo minutos!</h2>
                            <p>Selecciona entre los diferentes tipos de encuesta que tenemos para ti 
                                y envíalas a las personas que quieras en cuestión de <b>minutos</b>.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portafolio-container">
                        <img src={{asset("asset/images/MiPortafolio-03.jpg")}} class="img-fluid" alt="portafolio 1">
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="team" class="bgLightGrey">
        <div class="container">
            <div class="content-center">
                <h2>Tus proyectos <b>en buenas manos</b></h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="member-container">
                        <div class="member-details">
                            <h5>Juan Jose Ayala</h5>
                            <span>Product Owner</span>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-instagram"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-twitter"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-youtube"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-dribbble"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-facebook"></i></a></li>
                            </ul>
                        </div>
                        <img src="{{asset('asset/images/member-01.jpeg')}}" class="img-fluid" alt="member 1">
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="member-container">
                        <div class="member-details">
                            <h5>Yeison Mosquera</h5>
                            <span>Desarrollador</span>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-instagram"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-twitter"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-youtube"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-dribbble"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-facebook"></i></a></li>
                            </ul>
                        </div>
                        <img src="{{asset('asset/images/member-03.jpg')}}" class="img-fluid" alt="member 1">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="member-container">
                        <div class="member-details">
                            <h5>Jhonatan Montilla</h5>
                            <span>Desarrollador</span>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-instagram"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-twitter"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-youtube"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-dribbble"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="icon ion-logo-facebook"></i></a></li>
                            </ul>
                        </div>
                        <img src="{{asset('asset/images/member-02.jpg')}}" class="img-fluid" alt="member 1">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mt-4">
                    <div class="content-center">
                        <h3>¿Listo para acelerar tu proyecto?<br><b>Contáctanos Ahora</b></h3>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" placeholder="Last name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" class="form-control" id="" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" placeholder="Company name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="number" class="form-control" id="" placeholder="Phone number (optional)">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href="" class="btn btn-primary full-width">Contact sales</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="footer" class="bg-dark">
        <div class="container">
            <img src={{asset("asset/images/Logo3Blanco.png")}} class="logo-brand" alt="logo">
            <ul class="list-inline">
                <li class="list-inline-item footer-menu"><a href="{{route('index')}}">Home</a></li>
                <li class="list-inline-item footer-menu"><a href="#team">About Us</a></li>
                <li class="list-inline-item footer-menu"><a href="#contact">Contact</a></li>
            </ul>

            <small>©2020 All Rights Reserved. Created by SnakeSurvey</small>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>

</html>