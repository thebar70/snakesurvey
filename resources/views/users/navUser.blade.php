<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Jhonatan Montilla">
    <meta name="description" content="Dasboar evaluador">

    <title>SnakeSurvey</title>

    <!-- Bootstrap Css -->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <script src="{{asset('asset/bootstrap/js/core/jquery.3.2.1.min.js')}}"></script><!---->

    <!-- Hoja de estilos -->
    <link href="{{asset('asset/css/styles3.css')}}" rel="stylesheet">

    <!-- Google fonts -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Muli:400,700&display=swap')}}" rel="stylesheet">

    <!-- Ionic icons -->
    <link href="{{asset('https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css')}}" rel="stylesheet">

</head>

<body>

    <div class="d-flex" id="content-wrapper">

        <!-- Sidebar -->
        <div id="sidebar-container" class="bg-dark bg-light border-right">
            <div class="logo">
                <a class="navbar-brand" href="#"> <img src={{asset("asset/images/Logo3Blanco.png")}} class="logoSnake"
                        alt="logo"></a>
            </div>
            <div class="menu list-group-flush">
                <a href="{{route('index')}}"
                    class="list-group-item list-group-item-action text-muted p-3 border-0"><i
                        class="icon ion-md-add lead mr-2"></i> Mis Respuestas</a>
            </div>
        </div>
        <!-- Fin sidebar -->

        <!-- Page Content -->
        <div id="page-content-wrapper" class="w-100 bg-light-blue">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <div class="container">
                    <!-- boton de menu -->
                    <a href="#" id="menu-toggle"> <img src={{asset("asset/images/menu2.png")}}></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- Fin -->
                    <!-- Fin -->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <!-- Cerrar Sesion -->
                            <li class="nav-item dropdown">
                                <a class="nav-link text-dark dropdown-toggle" data-toggle="dropdown" href="#"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="avatar-sm">
                                        <img src="{{asset('asset/images/usuario.png')}}">
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-user animated fadeIn">
                                    <div class="dropdown-user-scroll scrollbar-outer">
                                        <li>
                                            <div class="user-box">
                                                <div class="avatar-lg"><img src="{{asset('asset/images/usuario.png')}}"
                                                        alt="image profile" class="avatar-img rounded"></div>
                                                

                                                <div class="u-text">
                                                    <h4>correo@usuario.com</h4>
                                                    <p class="text-muted">correo@usuario.com</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="dropdown-divider"></div>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                <button type="submit" class="btn-ghost tertiary round">
                                                    {{ __('Logout') }}
                                                </button>
                                                @csrf
                                            </form>

                                        </li>
                                    </div>
                                </ul>
                            </li>
                            </li>
                            <!-- Fin -->
                        </ul>
                    </div>
                </div> <!-- Fin Container -->
            </nav>
            <div class="main-panel">
                <div class="content">
                    @include('flash::message')
                    @yield('content')
        
                </div>
            </div>
        </div>
        
    </div>
    
    <!-- Fin Page Content -->
    
    <!-- Fin wrapper -->

    <!-- Bootstrap y JQuery -->
    <script src="{{asset('asset/js/jquery.js')}}"></script>
    <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>

</body>
<!-- Abrir / cerrar menu -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#content-wrapper").toggleClass("toggled");
    });
</script> 

</html>