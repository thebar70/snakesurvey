<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>¡Gracias!</title>

    <link rel="stylesheet" href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}">
    {{-- <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/atlantis.css')}}"> --}}

    <!-- Hoja de estilos -->
    <link href="{{asset('asset/css/estilos2.css')}}" rel="stylesheet">

    <!-- Google Fonds -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600&display=swap" rel="stylesheet">

</head>
<body>
    <div class="centrar">
        <h1 style="color: white;">¡Gracias por responder su Test!</h1>
    </div>

    <div class="centrar">
        <img class="logoSnake" src={{asset("asset/images/Logo3.png")}} alt="">
    </div>
</body>
</html>