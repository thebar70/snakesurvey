@extends('users.navUser')
@section('content')
<div id="content" class="container-fluid p-5">
    <section class="py-3">
        <!-- Highlights -->
        <div class="row">
            <form class="" action="{{route('store_evaluation')}}" method="POST" id="form_x"  data-parsley-validate
                class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="page-inner mt--5">
                    <div class="row mt--2">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">SnakeSurvey Evaluation</div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xs-12">
                                            <label for="email2">Nombre de la Encuesta</label>
                                            <input type="text" class="form-control" placeholder="{{$test->name}}" readonly>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xs-12">
                                            <label for="email2">Sitio</label>
                                            <input type="text" class="form-control" placeholder="{{$test->site}}" readonly>
                                        </div>
                                        <br><br><br><br><br>
                                        <div class="form-group col-md-6 col-lg-6 col-xs-3">
                                            
                                                <select class="form-control form-control-sm" name="gender" id="gender" required>
                                                    <option value="">Seleccione su genero...</option>
                                                    <option value="man">Hombre</option>
                                                    <option value="woman">Mujer</option>
                                                </select>
                                        </div>                                        <br><br><br>
                                        <div class="form-group col-md-6 col-lg-8 col-xs-12">
                                            <label for="email2">Tu fecha de nacimiento</label><br>
                                            <input type="date" name="birth_date" id="birth_date" required>
                                        </div>
                                        <br><br><br><br><br>
                                        <div class="form-group col-md-6 col-lg-10 col-xs-12">
                                            <label for="email2">Tus respueas calificaran este sitio</label>
                                            <a href="{{$test->site}}" target="_blank">{{$test->site}}</a>
                                        </div>
                                        <br><br><br><br>
                                        
                                        @foreach ($test->form->questions as $question)
                                        <div class="form-group col-md-6 col-lg-6 col-xs-3">
                                            <label for="smallSelect">{{$question->description}}</label>
                                            <select class="form-control form-control-sm" id="{{$question->id}}"
                                                name="{{$question->id}}" data-placeholder="Respuesta..." required>
                                                <option value="">Seleccione una opción...</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select> <br><br>
                                            @error('select.{{$question->id}}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        @endforeach
                                        <input type="hidden" name="user_token" value="{{$user_token}}">
                                    </div>
                                </div>
                                <div class="card-action">
                                    <div class="text text-center form-group col-md-6 col-lg-10 col-xs-12">
                                        <button id="btn-guardar" class="btn btn-success">Guardar mis respuestas</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br><br><br>
            </form>
        </div>
    </section>
</div>
<script>
  var answers=[];
  $('select').on('change', function() {
    if (this.id != 'gender') {
      answers.push(this.name +'-'+this.value);  
    }
    

  });
  $("#form_x" ).submit(function( event ) {

    var input = $("<input>")
      .attr("type", "hidden")
      .attr("name", "answers").val(answers);
      $('#form_x').append(input);
      document.getElementById("form_x").submit();
  });
</script>

@endsection