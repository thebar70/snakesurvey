<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}">
  {{-- <link rel="stylesheet"  href="{{asset('asset/bootstrap/css/atlantis.css')}}"> --}}

  <!-- Conectando con la hoja de estilos Css -->
  <link rel="stylesheet" href="{{asset('asset/css/estilos.css')}}">

  <!-- Google Fonds -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600&display=swap" rel="stylesheet">

  <!-- Ionicons -->
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
  <link href="{{asset('https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css')}}" rel="stylesheet">

  <title>Snake Survey</title>
</head>
@include('flash::message')
<body>
  <!-- Begin Page Content -->
  <div class="modal-dialog text-center">
      <!-- Utiliza toda la pantalla-->
      <div class="col-sm-12">
        <div class="modal-content">
          <div>
            <br>
            <a class="navbar-brand" href="{{route('index')}}"> <img src={{asset("asset/images/Logo3.png")}} class="logoSnake" alt="logo"></a>
            <br><br>
            <a href="{{route('login_google')}}" class="btn-ghost tertiary round" >CONTINUAR CON GOOGLE  <i2 class="icon ion-logo-google" ></i2></a>
            <br>
            <h6 class="efectoLinea"><b>o</b></h6>
          </div>
          <div>
            <br>
            <a href="{{route('login_facebook')}}" class="btn-ghost round">CONTINUAR CON FACEBOOK  <i class="icon ion-logo-facebook"></i></a>
            <br>
            <h6 class="efectoLinea"><b>o</b></h6>
            <h5><b>Regístrate con tu dirección de email</b>
          </div>
          <!-- Aquí empiezan los campos de la creación de usuario -->
          <form class="" action="{{route('store_person')}}" method="POST" id="form_x" data-parsley-validate class="form-horizontal form-label-left">
            {{ csrf_field() }}
            <div class="form-group row">
                            <div class="col-md-12">
                                <input id="names" type="text" class="form-control @error('names') is-invalid @enderror" name="names" value="{{ old('names') }}" autofocus placeholder="¿Cómo te llamas?">

                                @error('names')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmar Constraseña">
                            </div>
                        </div>
            <div class="form-group  col-md-12 col-md-6 col-xs-12">
                {{-- <h8>Fecha de Nacimiento</h8> --}}
              <label for="email2">Fecha Nacimiento</label>
              <input type="date" class="form-control number @error('birth_date') is-invalid @enderror"  id="birth_date" name="birth_date" placeholder="Fecha de Nacimiento" value="{{ old('birth_date') }}">
              @error('birth_date')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
                  <div class="col-md-6 offset-md-2">
                        <button type="submit" class="btn-ghost secundary round">
                             {{ __('Registrar') }}
                        </button>
                  </div>
            <br>
          </form>
        </div>
      </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>