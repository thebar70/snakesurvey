<?php

use Illuminate\Database\Seeder;
use App\Models\Form;
use App\Models\Question;
use App\Models\Option;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seed para el form de SUS
        $form = Form::create([
            'name' => 'SUS'
        ]);
        $array_questions = [
            1 => 'Creo que me gustaría usar este sistema con frecuencia',
            2 => 'Encontré el sistema innecesariamente complejo',
            3 => 'Pensé que el sistema era fácil de usar.',
            4 => 'Creo que necesitaría el apoyo de una persona técnica para poder utilizar este sistema.',
            5 => 'Encontré que las diversas funciones en este sistema estaban bien integradas.',
            6 => 'Pensé que había demasiada inconsistencia en este sistema.',
            7 => 'Me imagino que la mayoría de la gente aprendería a usar este sistema muy rápidamente',
            8 => 'El sistema me pareció muy engorroso de usar.',
            9 => 'Me sentí muy seguro usando el sistema.',
            10 => 'Necesitaba aprender muchas cosas antes de poder comenzar con este sistema.',
        ];
        foreach ($array_questions as $qts) {
            $new_question = Question::create([
                'form_id' => $form->id,
                'type' => 'multiple_option',
                'description' => $qts,
            ]);
            for ($i = 1; $i <= 5; $i++) {
                Option::create([
                    'question_id' => $new_question->id,
                    'description' => $i,
                ]);
            }
        }
        //Fin seed par Sus

       
    }
}
