<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'names' => 'Pedro picapiedra',
            'email' => 'pedro@snakesurvey.com',
            'password' => bcrypt('12345678'),
        ]);
        $user->roles()->attach(Role::where('name', 'evaluator')->first());
        
    }
}
